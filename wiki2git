#!/usr/bin/env python
"""
wikitogit converts a Wikipedia article into a Git repository

ex: ./wiki2git https://en.wikipedia.org/wiki/The_Little_Prince the_little_prince

# Git code inspired by the-law-factory-parser/tlfp/tools/make_git_repos.py
"""

import os
import shutil
import shlex
from pathlib import Path
import argparse
from urllib.parse import unquote

import requests
import untangle


def call(cmd, silent=True):
    if silent:
        cmd += '> /dev/null'
    code = os.system(cmd)
    if code != 0:
        raise Exception('"{}"" returned {}'.format(cmd, code))


def call_bash(cmd):
    call("bash -c " + shlex.quote(cmd))


def download_revisions(article_url, offset=None):
    base, page = article_url.split('/wiki/')
    headers = {
        'User-Agent': 'wiki2git/beta run by User:Dam-io (https://framagit.org/mdamien/wiki2git)',
    }
    resp = requests.post(
        base + '/wiki/Special:Export/',
        params={
            'pages': page,
            'offset': offset,
        },
        headers=headers
    )
    return resp.text


def main(args):
    url = unquote(args.url[0])

    # downloads all revisions
    all_revisions = []
    last_timestamp = None
    print('Downloading...')
    while True:
        tree = untangle.parse(download_revisions(url, offset=last_timestamp)).mediawiki
        revisions = list(tree.page.revision)
        all_revisions += revisions
        last_timestamp = all_revisions[-1].timestamp.cdata
        print('  .. received', len(revisions), 'revisions, total:', len(all_revisions))
        print('         ......', last_timestamp)
        if len(revisions) != 1000:
            break

    # makes git repo
    print('Converting revisions to commits...')
    git_dir = Path(args.dest)
    shutil.rmtree(str(git_dir), ignore_errors=True)
    os.makedirs(str(git_dir))

    call_bash(
        "(cd %s;" % git_dir.absolute()
        + "git init)"
    )

    for i, rev in enumerate(all_revisions):
        texte = rev.text.cdata
        text_dest = git_dir / "file"
        with open(str(text_dest), "w") as f:
            f.write(texte)
        author = '-'
        try:
            author = rev.contributor.username.cdata
        except:
            author = rev.contributor.ip.cdata

        message = "-"
        try:
            message = rev.comment.cdata
        except:
            pass

        date = rev.timestamp.cdata

        call_bash(
            "(cd %s" % git_dir.absolute()
            + "; git add *;"
            + "git status;"
            + "git config --local user.name %s;" % shlex.quote(author)
            + "git config --local user.email %s;" % shlex.quote('-')
            + " git commit --date=format:short:{} --author={} -m {} --allow-empty )".format(
                shlex.quote(date),
                shlex.quote(author + " <->"),
                shlex.quote(message + '\n\n' + url + "?oldid=%s&diff=prev" % rev.id.cdata),
            )
        )

        if i != 0 and i % 500 == 0:
            print('   ...', i, '/', len(all_revisions))


if __name__ == '__main__':
    cls = argparse.RawDescriptionHelpFormatter
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=cls)
    parser.add_argument('url', nargs=1, help='URL of the Wikipedia article')
    parser.add_argument('dest', nargs='?', default='repo', help='Location of the created git repository')
    args = parser.parse_args()

    main(args)
